<?php
interface Log
{
    public function write();
}

class FileLog implements Log
{
    public function write()
    {
        // TODO: Implement write() method.
        echo 'file log write';
    }
}

class User
{

    protected $log;

    public function __construct(FileLog $log)
    {
        $this->log = $log;
    }

    public function login()
    {
        echo 'login success';
        $this->log->write();
    }

}

function make($concrete){
    $reflector = new ReflectionClass($concrete);
    $constructor = $reflector->getConstructor();
    if(is_null($constructor)){
        return $reflector->newInstance();
    }else{
        $dependencies = $constructor->getParameters();
        $instences = getDependencies($dependencies);
    }
    return $reflector->newInstanceArgs($instences);
}

function getDependencies($parameters){
    $dependencies = [];
    foreach ($parameters as $parameter){
        $dependencies[] = make($parameter->getClass()->name);
    }
    return $dependencies;
}

$user = make('User');
$user->login();

