<?php
//定义写日志的规范
interface Log
{
    public function write();
}

//文件记录日志
class FileLog implements Log
{
    public function write()
    {
        // TODO: Implement write() method.
        echo 'file log write';
    }
}

//数据库记录日志
class DatabaseLog implements Log
{
    public function write()
    {
        // TODO: Implement write() method.
        echo 'database log write';
    }
}

class User
{

    protected $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    public function login()
    {
        echo '登陆成功，写入日志';
        $this->log->write();
    }

}

$user = new User(new FileLog());
$user->login();