| 单词        | 音标         | 译文                               |
| :---------- | :----------- | :--------------------------------- |
| reflection  | rɪˈflekʃn    | 反射                               |
| reflector   | rɪˈflektər   | n. 反光面                          |
| mapping     | mæpɪŋ        | 映射                               |
| constructor | kənˈstrʌktər | 制造者                             |
| instance    | ɪnstəns      | 实例                               |
| parameter   | pəˈræmɪtər   | 参数、范围、决定因素               |
| dependency  | dɪˈpendənsi  | 依赖                               |
| concrete    | kɑːŋkriːt    | adj.有形的、实在的  n.混泥土       |
| argument    | ɑːrɡjumənt   | n. 理由、论点   p.命令行参数       |
| abstract    | æbstrækt     | adj.抽象的                         |
| contract    | kɑːntrækt    | n.契约                             |
| facade      | fəˈsɑːd      | n.门面                             |
| accept      | əkˈsept      | v.接受、认可                       |
| access      | ækses        | v.访问  n.机会                     |
| duplicate   | duːplɪkeɪt   | adj.复制的、二重的  n.副本、复制品 |
| verify      | verɪfaɪ      | v.核实、证实                       |
| implement   | ɪmplɪmənt    | v.使生效、实施  n.工具             |
| pipe        | paɪp         | n.管子、管道  v、用管道输送        |
| stack       | stæk         | n.一堆                             |
| track       | træk         | v.追踪  n.踪迹                     |
| reduce      | rɪˈduːs      | 减少\降低                          |

​	













门面、外观	契约、合同	抽象的	论据、命令行参数	有形的、实体的	依赖	参数、决定因素	实例	制造者	映射	反光面	反射	接受	访问	复制的、二重的	管道	使生效	核实、证实	一堆	追踪、踪迹

facade	contract	abstract	argement	concrete	dependency	parameter	instance	constructor	mapping	reflector	reflection	accept	access	duplicate	pipe	implement	verify	stack	track