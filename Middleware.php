<?php
interface Middleware{
    public static function handle(Closure $next);
}

class VerifyCsrkToken implements Middleware {
    public static function handle(Closure $next)
    {
        echo "验证csrf token…\n";
        $next();
    }
}

class VerifyAuth implements Middleware{
    public static function handle(Closure $next)
    {
        echo "验证是否登录…\n";
        $next();
    }
}

class Cookie implements Middleware{
    public static function handle(Closure $next)
    {
        $next();
        echo "设置cookie…\n";
    }
}

$pipes = [
    'VerifyCsrkToken',
    'VerifyAuth',
    'Cookie'
];

$closure = array_reduce($pipes,function($stack,$pepi){
    return function() use($stack,$pepi){
        $pepi::handle($stack);
    };
},function(){
    echo "执行的程序\n";
});

$closure();